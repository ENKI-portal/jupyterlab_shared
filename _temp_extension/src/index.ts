import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

/**
 * Initialization data for the @enki-portal/shared extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: '@enki-portal/shared',
  autoStart: true,
  activate: (app: JupyterFrontEnd) => {
    console.log('JupyterLab extension @enki-portal/shared is activated!');
  }
};

export default extension;
