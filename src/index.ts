import {
  ILayoutRestorer, JupyterFrontEnd, JupyterFrontEndPlugin
} from '@jupyterlab/application';

import {
  IDocumentManager
} from '@jupyterlab/docmanager';

import {
  IFileBrowserFactory
} from '@jupyterlab/filebrowser';

import {
  ISettingRegistry
} from '@jupyterlab/settingregistry';

import '../style/index.css';

const NAMESPACE = 'shared-filebrowser';

const fileBrowserPlugin: JupyterFrontEndPlugin<void> = {
  id: '@enki-portal/shared:shared',
  requires: [IDocumentManager, IFileBrowserFactory, ILayoutRestorer, ISettingRegistry],
  activate: activateFileBrowser,
  autoStart: true
};

function activateFileBrowser(app: JupyterFrontEnd, manager: IDocumentManager, factory: IFileBrowserFactory, restorer: ILayoutRestorer, settingRegistry: ISettingRegistry): void {
  const id = fileBrowserPlugin.id;

  const browser = factory.createFileBrowser(NAMESPACE, {
    state:null
  });

  let location = '';
  let label = '';
  settingRegistry.load(id).then(settings => {
    location = settings.get('Location').composite as string;
    label = settings.get('Label').composite as string;
  });
  if (location !== '') {
    browser.model.cd(location);
  } else {
    browser.model.cd(browser.model.path+'/ThermoEngine/Notebooks'); 
  }
  if (label !== '') {
    browser.title.label = label;
  } else {
    browser.title.label = 'ENKI';
  }
  
  browser.id = 'shared-filebrowser';

  restorer.add(browser, NAMESPACE);
  app.shell.add(browser, 'left', { rank: 101 });

  return;
}

export default fileBrowserPlugin;
