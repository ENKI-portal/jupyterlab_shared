# Changelog

## Version 0.1.5 (2021-02-06)

- Update for JupyterLab 3.+

## Version 0.1.4 (2020-03-23)

- Update for JupyterLab 2.0
- Added gitlab-ci.yml for automated pipeline build

## Version 0.1.3 (2018-10-29)

- Update library and typescript dependencies

## Version 0.1.2 (2018-4-18)

- Update to JupyterLab 0.32

## Version 0.1.1 (2018-2-16)

- Initial release
