{
  "name": "@enki-portal/shared",
  "version": "0.1.4",
  "description": "A file browser implementation for accessing shared resources",
  "keywords": [
    "jupyterlab",
    "jupyter",
    "jupyterlab-extension"
  ],
  "homepage": "https://gitlab.com/enki-portal/jupyterlab_shared",
  "bugs": {
    "url": "https://gitlab.com/enki-portal/jupyterlab_shared/issues"
  },
  "license": "BSD-3-Clause",
  "author": "Mark S. Ghiorso",
  "files": [
    "lib/**/*.{d.ts,eot,gif,html,jpg,js,js.map,json,png,svg,woff2,ttf}",
    "schema/*.json",
    "style/**/*.{css,eot,gif,html,jpg,json,png,svg,woff2,ttf}",
    "style/index.js"
  ],
  "main": "lib/index.js",
  "types": "lib/index.d.ts",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/enki-portal/jupyterlab_shared.git"
  },
  "scripts": {
    "build": "jlpm run build:lib && jlpm run build:labextension:dev",
    "build:labextension": "jupyter labextension build .",
    "build:labextension:dev": "jupyter labextension build --development True .",
    "build:lib": "tsc",
    "build:prod": "jlpm run build:lib && jlpm run build:labextension",
    "clean": "jlpm run clean:lib",
    "clean:all": "jlpm run clean:lib && jlpm run clean:labextension",
    "clean:labextension": "rimraf enki_portal_shared/labextension",
    "clean:lib": "rimraf lib tsconfig.tsbuildinfo",
    "eslint": "eslint . --ext .ts,.tsx --fix",
    "eslint:check": "eslint . --ext .ts,.tsx",
    "install:extension": "jupyter labextension develop --overwrite .",
    "prepare": "jlpm run clean && jlpm run build:prod",
    "watch": "run-p watch:src watch:labextension",
    "watch:labextension": "jupyter labextension watch .",
    "watch:src": "tsc -w"
  },
  "dependencies": {
    "@jupyterlab/application": "^3.0.0",
    "@jupyterlab/docmanager": "^3.0.0",
    "@jupyterlab/filebrowser": "^3.0.0",
    "@jupyterlab/services": "^6.0.0",
    "@jupyterlab/settingregistry": "^3.0.0"
  },
  "devDependencies": {
    "@jupyterlab/builder": "^3.0.0-rc.13",
    "@typescript-eslint/eslint-plugin": "^2.27.0",
    "@typescript-eslint/parser": "^2.27.0",
    "eslint": "^7.5.0",
    "eslint-config-prettier": "^6.10.1",
    "eslint-plugin-prettier": "^3.1.2",
    "npm-run-all": "^4.1.5",
    "prettier": "^1.19.0",
    "rimraf": "^3.0.2",
    "typescript": "~4.1.3"
  },
  "sideEffects": [
    "style/*.css",
    "style/index.js"
  ],
  "jupyterlab": {
    "extension": true,
    "schemaDir": "schema",
    "outputDir": "enki_portal_shared/labextension"
  },
  "directories": {
    "lib": "lib"
  },
  "styleModule": "style/index.js"
}